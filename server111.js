var express = require("express"),
    app = express(),
    http = require("http").Server(app);

app.use(express.static(__dirname));

app.get("/", function(req, res) {
    res.sendFile(__dirname + "/index.html");
});

let port = 5000;
http.listen(port, "192.168.2.8", function() {
    console.log(` [ Tic-Tac-Toe Game ] Server running on port... ${port}`);
});

const io = require("socket.io")(http);

var players = {};
var matchWith = null;

function joinGame(socket, username) {
    players[socket.id] = {
        opponent: matchWith,
        symbol: "X",
        socket: socket
    };

    if (matchWith) {
        players[socket.id].symbol = "O";
        players[matchWith].opponent = socket.id;
        matchWith = null;
    } else {
        matchWith = socket.id;
    }
}

function getOpponent(socket) {
    if (!players[socket.id].opponent) {
        return;
    }
    return players[players[socket.id].opponent].socket;
}

io.on("connection", socket => {
    console.log("Connection established with...", socket.id);

    socket.on("username", (data) => {
        // data = username
        joinGame(socket, data);
        if (getOpponent(socket)) {
            socket.emit("game_begin", {
                symbol: players[socket.id].symbol
            });
            getOpponent(socket).emit("game_begin", {
                symbol: players[getOpponent(socket).id].symbol
            });
        }
    });

    socket.on("make_move", function(data) {
        if (!getOpponent(socket)) {
            return;
        }
        console.log("Move made by : ", data);
        socket.emit("move_made", data);
        getOpponent(socket).emit("move_made", data);
    });

    socket.on("disconnect", function() {
        if (getOpponent(socket)) {
            getOpponent(socket).emit("opponent_left");
        }
    });
});
