$(function() {
    var socket = io.connect(),
        myTurn = true,
        symbol,
        winCase = ["XXX", "OOO"];
    let myName = "",
        myOpponent = "";

    function isGameOver() {
        var state = {};
        $(".cell").each(function() {
            state[$(this).attr("id")] = $(this).text() || "";
        });

        var rows = [
            state.a0 + state.a1 + state.a2,
            state.b0 + state.b1 + state.b2,
            state.c0 + state.c1 + state.c2,
            state.a0 + state.b1 + state.c2,
            state.a2 + state.b1 + state.c0,
            state.a0 + state.b0 + state.c0,
            state.a1 + state.b1 + state.c1,
            state.a2 + state.b2 + state.c2
        ];

        for (var i = 0; i < rows.length; i++) {
            if (rows[i] === winCase[0] || rows[i] === winCase[1]) {
                return true;
            }
        }
        return false;
    }

    function setTurnMsg() {
        if (!myTurn) {
            $("#messages").text("Your opponent's turn");
            $(".cell").attr("disabled", true);
        } else {
            $("#messages").text("Your turn");
            $(".cell").removeAttr("disabled");
        }
    }

    function makeMove(e) {
        e.preventDefault();
        if (!myTurn) {
            return;
        }

        if ($(this).text().length) {
            return;
        }

        socket.emit("make_move", {
            name: myName,
            symbol: symbol,
            position: $(this).attr("id")
        });
    }

    function newGame(data) {
        $(".cell").empty();
        $("#myNameTitle").html(data.name);
        $("#symbol").html(data.symbol);
        $("#myOpponentName").html(data.opponent);
        myName = data.name;
        symbol = data.symbol;
        myOpponent = data.opponent;
        myTurn = data.symbol === "X";
        setTurnMsg();
        if (myTurn) {
            $(".cell").attr("disabled", false);
        }
    }

    function resetHtml() {
        $("#gameBoardArea").html(`<div id="countdown"></div>
        <div id="playAgainButton"></div>

        <div style="text-align: center;">
            <p id="myNameTitle">...</p> !  Your Symbol: <div id="symbol" >waiting...</div>
        </div>
         <div style="text-align: center;">
            Your Opponent: <div id="myOpponentName" >waiting...</div>
        </div>
        <br>
        <div class="board">
            <button class="cell btn btn-outline-info" id="a0"></button>
            <button class="cell btn btn-outline-info" id="a1"></button>
            <button class="cell btn btn-outline-info" id="a2"></button>
            <button class="cell btn btn-outline-info" id="b0"></button>
            <button class="cell btn btn-outline-info" id="b1"></button>
            <button class="cell btn btn-outline-info" id="b2"></button>
            <button class="cell btn btn-outline-info" id="c0"></button>
            <button class="cell btn btn-outline-info" id="c1"></button>
            <button class="cell btn btn-outline-info" id="c2"></button>
            <div id="messages">Waiting for opponent to join...</div>
        </div>`);
    }

    // game start username dilogue box
    $("#startGameButton").click(() => {
        username = $("#startGameInput")
            .val()
            .trim();
        if (username != "") {
            emit_username(username);
            $("#EnterUsername").hide();
            $("#gameBoardArea").show();
        }
    });
    $(".board button").attr("disabled", true);
    $(".cell").on("click", makeMove);

    socket.on("move_made", function(data) {
        // data = { name: 'self or opponent', symbol: 'X', position: A0 }
        $("#" + data.position).text(data.symbol);
        myTurn = data.symbol !== symbol;

        if (!isGameOver()) {
            return setTurnMsg();
        }

        if (myTurn) {
            $("#messages").text("Game over. You lost.");
        } else {
            $("#messages").text("Game over. You won!");
            if (confirm(`Play again with ${myOpponent}`)) {
                socket.emit("wantToPlayAgain");
            } else {
                socket.emit("decline");
                socket.emit("username", myName);
                newGame({ name: myName, symbol: "", opponent: "" });
                resetHtml();
            }
        }
        $(".cell").attr("disabled", true);
    });

    socket.on("decline", function() {});

    socket.on("wantToPlayQ", function() {
        if (confirm("play again with same opponent")) {
            socket.emit("palyAgainConfirm");
        } else {
            socket.emit("username", myName);
            newGame({ name: myName, symbol: "", opponent: "" });
            resetHtml();
        }
    });

    socket.on("newGameAgain", function(data) {
        // data = { name: 'myname', symbol: 'X', opponent:''}
        newGame(data);
    });

    // function startTimer() {
    //     var downloadTimer = setInterval(function() {
    //         $("#countdown").html(timeleft + " seconds remaining");
    //         timeleft -= 1;
    //         if (timeleft <= 0) {
    //             stopTimer(downloadTimer);
    //             socket.emit("timeout");
    //         }
    //     }, 1000);
    // }

    // function stopTimer(downloadTimer){
    //     clearInterval(downloadTimer);
    // }

    // socket.on("startTimer", function(data) {
    //     // console.log(data);
    //     // startTimer();
    // });

    socket.on("gameBegin", data => {
        // data = { name: 'myname', symbol: 'X', opponent:''}
        $("#myNameTitle").html(data.name);
        $("#symbol").html(data.symbol);
        $("#myOpponentName").html(data.opponent);
        myName = data.name;
        symbol = data.symbol;
        myOpponent = data.opponent;
        myTurn = data.symbol === "X";
        setTurnMsg();

        if (myTurn) {
            // timer();
        }
    });

    //  if the opponent lef => Disable the board
    socket.on("opponent_left", () => {
        $("#messages").text("Your opponent left the game.");
        $(".cell").attr("disabled", true);
        $("#playAgainButton").html(
            `<br><button type="button" class="btn btn-danger btn-block" onclick="window.location.reload();"> Play again</button>`
        );
    });

    // Socket Emit function
    function emit_username(username) {
        socket.emit("username", username);
    }
});
