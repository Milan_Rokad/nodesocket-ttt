// server setup
var express = require("express"),
    app = express(),
    http = require("http").Server(app);
app.use(express.static(__dirname));
app.get("/", function(req, res) {
    res.sendFile(__dirname + "/index.html");
});
let port = 5000;
http.listen(port, "192.168.2.8", function() {
    console.log(` [ Tic-Tac-Toe Game ] Server running on port... ${port}`);
});

// main application code
const io = require("socket.io")(http);
var time = new Date(),
    /* 
    players = { 
        a: { socket:{}, symbol: 'X', opponent: 'b'},
        b: { socket:{}, symbol: 'O', opponent: 'a'},
         ... 
    }
    */
    players = {},
    matchWith = null;

// pure js functions
function getOpponentByUsername(usr) {
    if (!players[usr].opponent) {
        return;
    }
    return players[usr].opponent;
}

function getUserBySocket(socket) {
    usr = Object.keys(players).find(
        username => players[username].socket === socket
    );
    return usr;
}

function joinGame(socket, username) {
    players[username] = {
        socket: socket,
        symbol: "X",
        opponent: matchWith
    };

    if (matchWith) {
        players[username].symbol = "O";
        players[matchWith].opponent = username;
        matchWith = null;
    } else {
        matchWith = username;
    }
}

// emit functions
function emit_gameBegin(socket, username) {
    // palyer with 'O' move
    socket.emit("gameBegin", {
        name: username,
        symbol: players[username].symbol,
        opponent: getOpponentByUsername(username)
    });

    // palyer with 'X' move
    players[getOpponentByUsername(username)].socket.emit("gameBegin", {
        name: getOpponentByUsername(username),
        symbol: players[getOpponentByUsername(username)].symbol,
        opponent: username
    });

}

// socket events
io.on("connection", socket => {
    console.log("\x1b[36m%s\x1b[0m", `New user Connected [${socket.id}]`);

    socket.on("username", username => {
        joinGame(socket, username);
        // if pair of player exist
        if (getOpponentByUsername(username)) {
            emit_gameBegin(socket, username);
        }
    });

    socket.on("make_move", function(data) {
        //  data = { name: '', symbol: 'X', position: A0 }
        if (!getOpponentByUsername(data.name)) {
            return;
        }
        console.log("Move made by : ", data);
        socket.emit("move_made", data);
        players[getOpponentByUsername(data.name)].socket.emit(
            "move_made",
            data
        );
    });

    socket.on("wantToPlayAgain", function() {
        user = getUserBySocket(socket);
        players[getOpponentByUsername(user)].socket.emit("wantToPlayQ");
    });

    socket.on("NotWantToPlayAgain", function() {
        user = getUserBySocket(socket);
        oppo = getOpponentByUsername(user);
        joinGame(socket, user);
        joinGame(socket, oppo);
    });

    socket.on("palyAgainConfirm", function() {
        user = getUserBySocket(socket);
        oppo = getOpponentByUsername(user);
        socket.emit("newGameAgain", {
            name: user,
            symbol: players[user].symbol,
            opponent: players[user].opponent
        });
        players[oppo].socket.emit("newGameAgain", {
            name: oppo,
            symbol: players[oppo].symbol,
            opponent: players[oppo].opponent
        });
    });

    socket.on("disconnect", socket => {
        disconnectedUser = getUserBySocket(socket);

        console.log(
            "\x1b[31m%s\x1b[0m",
            `User disconneted: [${disconnectedUser}     ${socket.id}]`
        );

        if (disconnectedUser !== undefined) {
            delete players[disconnectedUser];
            oppo = getOpponentByUsername(disconnectedUser);
            if (oppo) {
                players[oppo].opponent = null;
                players[oppo].socket.emit("opponent_left");
            }
        }
        // console.log(players);
    });
});
